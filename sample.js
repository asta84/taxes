let a = 2;
let b = 2;

console.log('Start');

// if (a + b === 4) { // a + b > 4 -> false because 1 + 2 < 4
//     console.log("a + b is 4", a + b);
// } else if ( a + b < 4) {
//     console.log("Please enter different values");    
// }

const value = "error";
const message = "Hello from the nodeJS";
switch (value) {
    case "error":
        console.log(`[Error] + ${message}`);
    case "info":
        console.log(`[INFO] + ${message}`);
        break;
    case "log":
        break;
    default:
        console.log(`${message}`);
}

//option 1 start + the end


console.log('The End');



function sumOfSquares(x, y) {
    console.log('x, y is ',x, y);
    let a = x * x;
    console.log('a:', a);
    let b = y * y;
    console.log('b:', b);
    console.log('a + b < 100', a + b < 100);
    if ( a + b < 100) {
        console.log('a + b:', a + b);
        return a + b;
    } else {
        console.log('a - b:', a - b);
        return a - b;
    }
}

const a = 10
const value = sumOfSquares(a, 6);
const result = console.log(value, a);
console.log(result);


if ( { key: "value"} === {  key: "value" }) {
    console.log('true');
} else {
    console.log('false');
}


const array = [0,1,2,3,4,5,6,7,8,9];

// for (let i = 0; i < 10; i = i + 1) {
//     console.log(`${array[i]}) square is ${array[i] * array[i]}`);
// }

let i = 0;
while (i < 10) {
    console.log(`${array[i]}) square is ${array[i] * array[i]}`);
    i = i + 1;
}

// let i = 0;
// do {
//     console.log(`${array[i]}) square is ${array[i] * array[i]}`);
//     i = i + 1;
// } while (i < 10)

array.forEach(function(item) {
    console.log(`${item}) square is ${item *item}`);
});



const Tbool = true;
const Fbool = false;
const text = "Some text goes here";
const otherText = "I'm more text coming here";
const pattern = `text:${text},\notherText:${otherText}`;

const array = ["a", "b", "c"];
let iterator = 0;
// console.log(Tbool, Fbool);
// console.log(text);
// console.log(otherText);
// console.log(`my array on pos ${iterator} is ${array[iterator + 1]}`);

const human = {
    name: "Janis",
    surname: "Knets",
    age: 33,
    children: [{
        name: "Kaspars"
    },{
        name: "Agate"
    }, 4 , "text"]
}

const UserName = human.name; //copy by value;
const age = human.age // copy value;
const children = human.children; //copy by refrence;

children[2] = {
    name: "Oskars"
}
// option1 - error, children is a const // error
// option2 - children would be changed for both children and human.children // name Oskars
// option3 - do insert for oskars in to children // 4

console.log(human.children);
console.log(children);



console.log('hello world');

// var a = "text";
// var a = "other text";
// console.log(a);

let c = 4;
console.log(c);
c = "four";
console.log(c);
c = [4,4,4,4];
console.log(c);
c = { four: 4 };
console.log(c);
