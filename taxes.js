
function taxes_paid_LV(x)
{
	social_LV = x * 0.105;
	yearly_LV = x * 12;
	if (yearly_LV < 20004)
		income_tax_LV = x *0.2
	else if (yearly_LV >= 20004 && yearly_LV < 62800)
		income_tax_LV = x * 0.23;
	else 
		income_tax_LV = x * 0.31;
	total_tax_paid_LV = income_tax_LV + social_LV;
	return(total_tax_paid_LV);
}
//console.log("total taxes paid:");
//console.log(taxes_paid_LV(1400));
function netto_sallary_LV(x)
{
	social_LV = x * 0.105;
		yearly_LV = x * 12;
		if (yearly_LV < 20004)
			income_tax_LV = x *0.2
		else if (yearly_LV >= 20004 && yearly_LV < 62800)
			income_tax_LV = x * 0.23;
		else 
			income_tax_LV = x * 0.31;
		total_tax_paid_LV = income_tax_LV + social_LV;
		netto_sallary = x - total_tax_paid_LV;
		return(netto_sallary);
}	
//console.log("netto salary:");
//console.log(netto_sallary_LV(1400));

function salary_netto_LT(y, accidents_at_work_group)
{
switch (accidents_at_work_group) {
	case "1":
		accidents_at_work_LT = 0.0014;
		break;
	case "2":
		accidents_at_work_LT = 0.0047;
		break;
	case "3":
		accidents_at_work_LT = 0.007;	
		break;
	case "4":
		accidents_at_work_LT = 0.014;
		break;
}
	income_tax_LT = y * 0.2;
	pension_insurance = y * 0.0872;
	sickness = 0.029 * y;
	maternity = 0.0171 * y;
	unemployment = 0.0131 * y
	health_insurance = sickness + maternity + unemployment;
	accidents_at_work_tax = y * accidents_at_work_LT;
	fund = y * 0.0016;
	long_term_employment = y * 0.005;
	return(y - income_tax_LT - pension_insurance - health_insurance - accidents_at_work_tax - fund -long_term_employment);
}
//console.log("netto sallary:")
//console.log(salary_netto_LT(2000, "1"));
function total_taxes_LT(y, accidents_at_work_group)
{
switch (accidents_at_work_group) {
	case "1":
		accidents_at_work_LT = 0.0014;
		break;
	case "2":
		accidents_at_work_LT = 0.0047;
		break;
	case "3":
		accidents_at_work_LT = 0.007;	
		break;
	case "4":
		accidents_at_work_LT = 0.014;
		break;
}
	income_tax_LT = y * 0.2;
	pension_insurance = y * 0.0872;
	sickness = 0.029 * y;
	maternity = 0.0171 * y;
	unemployment = 0.0131 * y
	health_insurance = sickness + maternity + unemployment;
	accidents_at_work_tax = y * accidents_at_work_LT;
	fund = y * 0.0016;
	long_term_employment = y * 0.005;
	return (income_tax_LT + pension_insurance + health_insurance + accidents_at_work_tax + fund + long_term_employment);
}
//	console.log("taxes paid LT:")
//	console.log(total_taxes_LT(2000, "1"));
function employee(name, surname)
{
	switch (name || surname){
		case "A":
			console.log("total taxes paid:");
			console.log(taxes_paid_LV(1000));				
			console.log("netto salary:");
			console.log(netto_sallary_LV(1000));	
			break;
		case "B":
			console.log("total taxes paid:");
			console.log(taxes_paid_LV(1400));				
			console.log("netto salary:");
			console.log(netto_sallary_LV(1400));	
			break;
		case "C":
			console.log("taxes paid LT:")
			console.log(total_taxes_LT(2000, "1"));			
			console.log("netto sallary:")
			console.log(salary_netto_LT(2000, "1"));
			break;
		case "D":
			console.log("total taxes paid:");
			console.log(taxes_paid_LV(1800));				
			console.log("netto salary:");
			console.log(netto_sallary_LV(1800));	
			break;
		case "E":
			console.log("taxes paid LT:")
			console.log(total_taxes_LT(2400, "2"));			
			console.log("netto sallary:")
			console.log(salary_netto_LT(2400, "2"));	
			break;
		case "F":
			console.log("total taxes paid:");
			console.log(taxes_paid_LV(3000));				
			console.log("netto salary:");
			console.log(netto_sallary_LV(3000));	
			break;
		case "G":
			console.log("taxes paid LT:")
			console.log(total_taxes_LT(1000, "2"));			
			console.log("netto sallary:")
			console.log(salary_netto_LT(1000, "2"));
			break;
		default:
		  console.log("enter name or surname");
		  break;
	}
}
console.log(employee("A"));

/* pension LT should be counted including pension insurance with 3 options: 2.4, 3 and 0 % (need to check by laws as in task it is
	not clear why options given but already counted pension insurance 8.72% )
create a table to make query by name and surname;
update sallary field;
check (query) and compare field in a table "LV" or "LT"
apply taxes counting system according to countryy and/or insurance group, pension insurance rate
post counted values;
*/
